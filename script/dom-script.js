let idCount = 0;
let tasks = [];
const urlSun = "../images/icon-sun.svg";
const urlMoon = "../images/icon-moon.svg";
const urlBackgroundLight = "../images/bg-desktop-light.jpg";
const urlBackgroundDark = "../images/bg-desktop-dark.jpg";

const newItem = document.getElementById("new-item");
const taskList = document.getElementById("list-items");
const tasksCounter = document.getElementById("counter");
const clickEvents = document.getElementById("container");
clickEvents.addEventListener("click", handelClickEvents);

let totalTasks = newItem.addEventListener("keyup", addValue);
function addValue(event) {
  if (event.key === "Enter") {
    let newTask = newItem.value;
    if (newTask.length === 0) {
      return;
    }
    let task = {
      title: newTask,
      id: idCount,
      completed: false,
    };
    idCount = idCount + 1;

    addTask(task);
    newItem.value = "";
  }
}

// adding a new task to tasks list
function addTask(task) {
  if (task) {
    tasks.push(task);
    renderList();
  }
}

// using tasks array to add task to taskList
function renderList() {
  taskList.innerHTML = "";

  for (let i = 0; i < tasks.length; i++) {
    addTaskToListItem(tasks[i]);
  }
  tasksCounter.innerHTML = `${tasks.length} items left`;
}

// adding task to Task List
function addTaskToListItem(task) {
  const li = document.createElement("li");
  li.classList.add("li-item");
  li.innerHTML = `
  <input type="checkbox" id="${task.id}" ${
    task.completed ? "checked" : ""
  } data-id="${task.id}" class="custom-checkbox">
  <span class="tick-mark"><img src="./images/icon-check.svg" alt=""></span>
  <label for="${task.id}">${task.title}</label>
  <img src="../images/icon-cross.svg" class="delete" data-id="${
    task.id
  }" alt="delete" />
`;

  taskList.append(li);
}
// Delete task using cross button
function deleteTask(taskId) {
  const newTasks = tasks.filter((task) => task.id != taskId);
  tasks = newTasks;
  renderList();
}

// render all tasks
function renderAll() {
  const newTasks = tasks.filter((task) => task.completed == false);
  renderList();
  tasksCounter.innerHTML = `${newTasks.length} items left`;
}

// render complete tasks
function renderComplete() {
  taskList.innerHTML = "";

  const newTasks = tasks.filter((task) => task.completed == true);
  for (let i = 0; i < newTasks.length; i++) {
    addTaskToListItem(newTasks[i]);
  }
  tasksCounter.innerHTML = `${newTasks.length} tasks completed`;
}

//render Active tasks
function renderActive() {
  taskList.innerHTML = "";
  const newTasks = tasks.filter((task) => task.completed == false);
  for (let i = 0; i < newTasks.length; i++) {
    addTaskToListItem(newTasks[i]);
  }
  tasksCounter.innerHTML = `${newTasks.length} tasks to complete`;
}

// for checking and unchecking individual task
function toggleTask(taskId) {
  let updatedCount = 0;
  let newTaskArray = tasks.map((task) => {
    if (task.id == taskId) {
      task.completed = !task.completed;
    }
    if(task.completed == false){
      updatedCount+=1;
      return task;
    } 
    else {
      return task;
    }
  });
  renderList();
  tasksCounter.innerHTML = `${updatedCount} tasks left`;

}


// for removing completed tasks
function deleteCompleted() {
  const newTasks = tasks.filter((task) => task.completed == false);
  tasks = newTasks;
  renderList();
}

// for changing theme
function themeChanger(image) {
  document.body.classList.toggle("body-background-dark");
  let imageSource = image.getAttribute("src");
  let backgroundImage = document.getElementById("theme-image");
  if (imageSource == urlSun) {
    image.setAttribute("src", urlMoon);
    backgroundImage.setAttribute("src", urlBackgroundLight);
    changeBoxColorLight();
  } else {
    image.setAttribute("src", urlSun);
    backgroundImage.setAttribute("src", urlBackgroundDark);
    changeBoxColorDark();
  }
}

// for changing box-colors
function changeBoxColorDark() {
  let boxColor = document.querySelectorAll(".box-light");
  for (let i = 0; i < boxColor.length; i++) {
    boxColor[i].classList = "box-dark";
  }
}
// for changing box-colors
function changeBoxColorLight() {
  let boxColor = document.querySelectorAll(".box-dark");
  for (let i = 0; i < boxColor.length; i++) {
    boxColor[i].classList = "box-light";
  }
}

//all click events on container
function handelClickEvents(e) {
  const target = e.target;
  if (target.id == "theme-image") {
    themeChanger(target);
  } else if (target.className == "delete") {
    const taskId = target.dataset.id;
    deleteTask(taskId);
    return;
  } else if (target.className == "clear-all") {
    deleteCompleted();
    return;
  } else if (target.className == "custom-checkbox") {
    const taskId = target.id;
    toggleTask(taskId);
    return;
  } else if (target.id == "All") {
    renderAll();
    return;
  } else if (target.id == "Completed") {
    renderComplete();
    return;
  } else if (target.id == "Active") {
    renderActive();
    return;
  }
}
